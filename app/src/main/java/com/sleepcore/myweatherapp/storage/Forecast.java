package com.sleepcore.myweatherapp.storage;

/**
 * Created by Mark Thompson on 10/2/2018.
 * Copyright © 2018 SleepScore Labs. All rights reserved.
 */
public class Forecast {
    private float temperatureHigh;
    private float temperatureLow;
    private long temperatureHighTime;
    private long temperatureLowTime;
    private long temperatureAtOnePm;
    private String summary;

    //TODO: serialize using GSON
    //TODO: will need to make a seperate request for temperature at 1pm, so I would merge two forecast objects together
}
