package com.sleepcore.myweatherapp.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by Mark Thompson on 10/2/2018.
 * Copyright © 2018 SleepScore Labs. All rights reserved.
 */
public class ForecastAdapter extends RecyclerView.Adapter {

    //TODO: in here, use the card_forecast as the view and update the values

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
