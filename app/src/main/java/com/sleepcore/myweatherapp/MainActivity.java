package com.sleepcore.myweatherapp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.sleepcore.myweatherapp.storage.Forecast;
import com.sleepcore.myweatherapp.ui.ForecastAdapter;

import java.util.ArrayList;

/**
 * Created by Mark Thompson on 10/2/2018.
 * Copyright © 2018 SleepScore Labs. All rights reserved.
 */
public class MainActivity extends Activity {
    private static final int FIVE_MINS_IN_MS = 300000;
    private TextView mTvAPICounter;
    private RecyclerView mRvForecasts;
    private ForecastAdapter mForecastAdapter;
    private Handler handler = new Handler();
    private int mAPIRequestCount = 0;
    private ArrayList<Forecast> mForecasts = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIElements();
        handler.postDelayed(mRequestUpdate, FIVE_MINS_IN_MS);

    }

    private void setUIElements() {
        mTvAPICounter = findViewById(R.id.tvApiRequestCounter);
        mRvForecasts = findViewById(R.id.rvForecasts);
        mRvForecasts.setAdapter(mForecastAdapter);
    }

    private void getLatestWeather() {

    }

    private Runnable mRequestUpdate = new Runnable() {
        @Override
        public void run() {
            String apiCounter = getString(R.string.api_counter) + mAPIRequestCount;
            //TODO: if request succeeds, update the counter, otherwise don't update
            //TODO: submit requests here
            //TODO: because we need to track all API requests, store the count in shared preferences

            mTvAPICounter.setText(apiCounter);
        }
    };
}
