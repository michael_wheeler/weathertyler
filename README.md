# weatherTyler
https://darksky.net/dev/login
#####username: 
michael.wheeler@sleepscorelabs.com 
#####password: 
Abcd1234
#####API Key: 
dc6b400d954930bdc95d08e095504048

#Sample API Call:
https://api.darksky.net/forecast/dc6b400d954930bdc95d08e095504048/37.8267,-122.4233

#Please refer to their documentation for more details:
https://darksky.net/dev/docs

#Minimum Task:
Create an app that displays todays current weather and the next 7 day forecast in Paris, France (48.8566° N, 2.3522° E)

1. Each day needs to have:
	* High temperature - with time marked
	* Low temperature - with time marked
	* Mid-day temperature - 1pm
	* Projected Weather conditions
2. Every 5 minutes, the UI should update without user intervention for current weather. 
3. Every API request increments a counter that is also shown in the UI.
	* For example: after 3 requests, your UI shows counter = 3

#If you have time:
1. Upon terminating the app, your API requests should continue. 
	*When the app resumes and is in the foreground, the UI should display current number of requests including those posted while the app was terminated.
2. Need to be able to select any of the days to see more details
3. Display Pictures to represent the weather forecast
4. Have ability to switch between current location and Paris.

#Things to consider:
1. App should not crash
1. No need to cater for edge cases 
1. Plain UI is acceptable (but the data should be easily to understand, with noticeable differences for the days)
